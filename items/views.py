from django.shortcuts import render, HttpResponse
from .models import Category, Item
from django.db.models import Q

# Create your views here.
def displayItems(request):
    query = request.GET.get('query', '')
    category_id = request.GET.get('category', 0)
    categories = Category.objects.all()
    items = Item.objects.filter(is_sold = False)
    if category_id:
        items = items.filter(category_id = category_id)
    if query:
        items = items.filter(Q(name_icontains = query) | Q(description_icontains=query))
    
    return HttpResponse('Hello World')