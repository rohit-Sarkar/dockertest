from django.urls import path, include
from . import views

urlpatterns = [
    path('items_display/', views.displayItems, name='displayItems'),
]
