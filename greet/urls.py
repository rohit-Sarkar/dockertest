from django.urls import path, include
from . import views

urlpatterns = [
    path('greet/', views.hello, name='greet'),
    path('newgreet/', views.helloUser, name='newgreet'),
]
