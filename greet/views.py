from django.shortcuts import render, HttpResponse

# Create your views here.
def hello(request):
    return HttpResponse('This is the index page of the application')

def helloUser(request):
    return HttpResponse('Hello user')